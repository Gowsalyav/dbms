package web1;

public class StudentVo {
	private String adm_no1;
	private String f_name1;
	private String l_name1;
	private String dob1;
	private String gender1;
	private String roll_no1;

	public String getAdm_no1() {
		return adm_no1;
	}

	public void setAdm_no1(String adm_no1) {
		this.adm_no1 = adm_no1;
	}

	public String getF_name1() {
		return f_name1;
	}

	public void setF_name1(String f_name1) {
		this.f_name1 = f_name1;
	}

	public String getL_name1() {
		return l_name1;
	}

	public void setL_name1(String l_name1) {
		this.l_name1 = l_name1;
	}

	public String getDob1() {
		return dob1;
	}

	public void setDob1(String dob1) {
		this.dob1 = dob1;
	}

	public String getGender1() {
		return gender1;
	}

	public void setGender1(String gender1) {
		this.gender1 = gender1;
	}

	public String getRoll_no1() {
		return roll_no1;
	}

	public void setRoll_no1(String roll_no1) {
		this.roll_no1 = roll_no1;
	}
}
