<%@page import="web1.StudentVo"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="#">
</head>
<%
	Map<String, StudentVo> map = (Map<String, StudentVo>) session.getAttribute("mp");
%>
<body>
	<center>
		<h1>Student Edit Application</h1>
		<br>
		<table border = "1">
			<tr>
				<th>Admission No</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Date of Birth</th>
				<th>Gender</th>
				<th>Roll No</th>
				<th>Modify</th>
				<th>Delete</th>
			</tr>
			<%
				for (String s : map.keySet()) {
					StudentVo x = map.get(s);
			%>
			<tr>
				<td><%=x.getAdm_no1()%></td>
				<td><%=x.getF_name1()%></td>
				<td><%=x.getL_name1()%></td>
				<td><%=x.getDob1()%></td>
				<td><%=x.getGender1()%></td>
				<td><%=x.getRoll_no1()%></td>
				<td><a href="StudentEdit.jsp?id1=<%=x.getAdm_no1()%>">Edit</a></td>
				<td><a href="StudentDelete.jsp?id1=<%=x.getAdm_no1()%>">Delete</a></td>
			</tr>
			<%
				}
			%>
		</table>
	</center>

</body>
</html>



