package web1;

import java.util.Map;

public interface InfStudentBo {

	public void getDetails(StudentVo obj) throws Exception;

	public Map<String, StudentVo> getDetails() throws Exception;
}
